# Sets doesn't accept dupplicated items

s = set([1, 2, 3])
t = set([3, 4, 5])
print(type(s)) # <class 'set'>

print(s.union(t)) # {1, 2, 3, 4, 5}

print(s.intersection(t)) # {3}

print(s.difference(t)) # {1, 2}
print(t.difference(s)) # {4, 5}

print(1 in s) # True
print(1 in t) # False
print(1 not in s) # False

print(s | t) # {1, 2, 3, 4, 5}
print(s & t) # {3}
print(s-t) # {1, 2}
print(s ^ t) # {1, 2, 4, 5}

countries = {'col', 'mex', 'bol'}
print(type(countries)) # <class 'set'>




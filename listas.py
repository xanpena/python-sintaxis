# -+- coding: utf-8 -*-
colores = list()
colores.append('rojo')
colores.append('azul')
colores.append('amarillo')

colores.insert(2, "verde")

print(colores[:])
print(colores)

print(colores.index("rojo"))

colores.extend(['Marron', 'naranja', 'Violeta'])

colores.remove("amarillo")

colores.pop()

print(colores)
print(colores[1])

print(type(colores)) # <class 'list'>
id(colores)

colores.sort(key=str.lower)

types = [1, True, 'Python']
print(type(types)) # <class 'list'>

types.append(700)
print(types) # [1, True, 'Python', 700]

types.insert(0, 'Hello')
print(types) # ['Hello', 1, True, 'Python', 700]

tasks = ['Todo 1', 'Todo 2']
newList = types + tasks
print(newList) # ['Hello', 1, True, 'Python', 700, 'Todo 1', 'Todo 2']

newList.pop()
print(newList) # ['Hello', 1, True, 'Python', 700, 'Todo 1']

newList.pop(0)
print(newList) # [1, True, 'Python', 700, 'Todo 1']

newList.remove('Todo 1')
print(newList) # [1, True, 'Python', 700]